import React, { useState } from 'react';
import { Anchor, Drawer, Button } from 'antd';

const { Link } = Anchor;
function AppHeader() {
  const [visible, setVisible] = useState(false);

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  return (
    <div className="container-fluid">
      <div className="header">
        <div className="logo">
          <a href="#" style={{ fontFamily: "cursive", fontWeight: "bold" }}>Grill & Chill</a>
        </div>

        <div className="mobileHidden">
          <Anchor targetOffset="65">
            <Link href="#main" title="Home" />
            <Link href="#about" title="About" />
            <Link href="#gallery" title="Food Gallery" />
            <Link href="#feedback" title="Feedback" />
          </Anchor>
        </div>
        
        <div className="mobileVisible">
          <Button type="primary" onClick={showDrawer}>
            <i className="fas fa-bars"></i>
          </Button>
          <Drawer
            placement="right"
            closable={false}
            onClose={onClose}
            visible={visible}
          >
            <Anchor targetOffset="65">
              <Link href="#main" title="Home" />
              <Link href="#about" title="About" />
              <Link href="#feedback" title="Feedback" />
            </Anchor>
          </Drawer>
        </div>
      </div>
    </div>
  );
}

export default AppHeader;