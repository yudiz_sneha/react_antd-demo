import React from 'react';
import { Form, Input, Button } from 'antd';
const { TextArea } = Input;

function Feedback() {
  return (
    <div id="feedback" className="block contactBlock">
      <div className="container-fluid">
        <div className="titleHolder">
          <h2>Feedback</h2>
        </div>

        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
        >

          {/* FullName */}
          <Form.Item
            name="fullname"
            rules={[
              { 
                required: true,
                message: 'Please enter your Fullname!' 
              }]
            }
          >
            <Input placeholder="FullName" />
          </Form.Item>

            {/* Email */}
          <Form.Item
            name="email"
            rules={[
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
            ]}
          >
            <Input placeholder="Email Address"/>
          </Form.Item>

            {/* Mobile */}
          <Form.Item
            name="mobile"
          >
            <Input placeholder="Mobile" />
          </Form.Item>

          {/* Feedback */}
          <Form.Item
            name="feedback"
          >
            <TextArea placeholder="Feedback" />
          </Form.Item>

            {/* Submit */}
          <Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>  
  );
}

export default Feedback;