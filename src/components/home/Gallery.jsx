import React from 'react';
import image1 from '../../assets/images/asnim-ansari-SqYmTDQYMjo-unsplash.jpg';
import image2 from '../../assets/images/asnim-ansari-SqYmTDQYMjo-unsplash.jpg';
import image3 from '../../assets/images/asnim-ansari-SqYmTDQYMjo-unsplash.jpg';
import image4 from '../../assets/images/asnim-ansari-SqYmTDQYMjo-unsplash.jpg';
import image5 from '../../assets/images/asnim-ansari-SqYmTDQYMjo-unsplash.jpg';
import image6 from '../../assets/images/asnim-ansari-SqYmTDQYMjo-unsplash.jpg';
import { Row, Col } from 'antd';
import { Card } from 'antd';
const { Meta } = Card;

function Gallery() {
  return (
    <div id="gallery" className="block featureBlock bgGray">
      <div className="container-fluid">
        <div className="titleHolder">
          <h2>Food Gallery</h2>
        </div>

        <Row gutter={[16, 16]}>
          <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }}>
            <Card
              hoverable
              cover={<img alt="Sandwich" src={image1} />}
            >
              <Meta title="Sandwich" />
            </Card>
          </Col>

          <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }}>
            <Card
              hoverable
              cover={<img alt="Sandwich" src={image2} />}
            >
              <Meta title="Sandwich" />
            </Card>
          </Col>

          <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }}>
            <Card
              hoverable
              cover={<img alt="Sandwich" src={image3} />}
            >
              <Meta title="Sandwich" />
            </Card>
          </Col>

          <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }}>
            <Card
              hoverable
              cover={<img alt="Sandwich" src={image4} />}
            >
              <Meta title="Sandwich" />
            </Card>
          </Col>

          <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }}>
            <Card
              hoverable
              cover={<img alt="Sandwich" src={image5} />}
            >
              <Meta title="Sandwich" />
            </Card>
          </Col>
          
          <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }}>
            <Card
              hoverable
              cover={<img alt="Sandwich" src={image6} />}
            >
              <Meta title="Sandwich" />
            </Card>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default Gallery;