import React from 'react';
import { Row, Col } from 'antd';

const items = [
  {
    key: '1',
    title: 'Best Taste',
  },
  {
    key: '2',
    title: 'Good Quality',
  },
  {
    key: '3',
    title: '100% Safety',
  },
]

function About() {
  return (
    <div id="about" className="block aboutBlock">
      <div className="container-fluid">
        <div className="titleHolder">
          <h2>About Us</h2>
        </div>
        <div className="contentHolder">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit necessitatibus officiis repudiandae est deserunt delectus dolorem iure porro distinctio fuga, nostrum doloremque. Facilis porro in laborum dolor amet ratione hic? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam aut a porro, adipisci quidem sint enim pariatur ducimus, saepe voluptatibus inventore commodi! Quis, explicabo molestias libero tenetur temporibus perspiciatis deserunt.</p>
        </div>
        <Row gutter={[16, 16]}>   
          {items.map(item => {
            return (
              <Col md={{ span: 8 }} key={item.key}>
                <div className="content">
                  <h3>{item.title}</h3>
                </div>
              </Col>
            );
          })}
        </Row>
      </div>
    </div>
  );
}

export default About;