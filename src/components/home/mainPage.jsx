import React from 'react';
import { Carousel } from 'antd';

const items = [
  {
    key: '1',
    title: 'Welcome to Grill & Chill Restaurant',
  },
  {
    key: '2',
    title: 'Welcome to Grill & Chill Restaurant',
  },
  {
    key: '3',
    title: 'Welcome to Grill & Chill Restaurant',
  },
]

function MainPage() {
  return (
    <div id="main" className="mainBlock">
      <Carousel>
        {items.map(item => {
          return (
            <div key={item.key} className="container-fluid">
              <div className="content">
                <h3 style={{ textAlign: "center", fontFamily: "cursive", fontWeight: "bold" }}>{item.title}</h3>
              </div>
            </div>  
          );
        })}
      </Carousel>
    </div>
  );
}

export default MainPage;