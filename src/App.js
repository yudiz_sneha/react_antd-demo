import React from 'react';
import './App.css';
import 'antd/dist/antd.css';
import AppHeader from './components/common/Header';
import Home from './views/index';
import { Layout } from 'antd';
const { Header, Content } = Layout;

function App() {
  return (
    <Layout className="mainLayout">
      <Header>
        <AppHeader/>
      </Header>
      <Content>
        <Home/>
      </Content>   
    </Layout>
  );
}

export default App;
