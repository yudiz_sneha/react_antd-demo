import React from 'react';
import MainPage from '../components/home/mainPage';
import About from '../components/home/About';
import Gallery from '../components/home/Gallery';
import Feedback from '../components/home/Feedback';

function Home() {
  return (
    <div className="main">
      <MainPage />
      <About />
      <Gallery />
      <Feedback />
    </div>
  );
}

export default Home;